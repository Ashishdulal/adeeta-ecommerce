<!DOCTYPE html>
<html lang="en">
<head><!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="Anil z" name="author">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content=""><!-- SITE TITLE -->
    <title>Adeeta Online </title><!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png"><!-- Animation CSS -->
    <link rel="stylesheet" href="css/css-animate.css"><!-- Latest Bootstrap min CSS --><link rel="stylesheet" href="css/css-bootstrap.min.css"><!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet"><!-- Icon Font CSS -->
    <link rel="stylesheet" href="css/css-all.min.css">
    <link rel="stylesheet" href="css/css-ionicons.min.css">
    <link rel="stylesheet" href="css/css-themify-icons.css">
    <link rel="stylesheet" href="css/css-linearicons.css">
    <link rel="stylesheet" href="css/css-flaticon.css">
    <link rel="stylesheet" href="css/css-simple-line-icons.css"><!--- owl carousel CSS-->
    <link rel="stylesheet" href="css/css-owl.carousel.min.css">
    <link rel="stylesheet" href="css/css-owl.theme.css">
    <link rel="stylesheet" href="css/css-owl.theme.default.min.css"><!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="css/css-magnific-popup.css"><!-- Slick CSS -->
    <link rel="stylesheet" href="css/css-slick.css">
    <link rel="stylesheet" href="css/css-slick-theme.css"><!-- Style CSS -->
    <link rel="stylesheet" href="css/css-style.css">
    <link rel="stylesheet" href="css/css-responsive.css"> 

<!-- START HEADER -->
<?php include('include_header1.php') ?>
<!-- END HEADER --><!-- START SECTION BREADCRUMB -->
<div class="breadcrumb_section bg_gray page-title-mini">
    <div class="container"><!-- STRART CONTAINER -->
        <div class="row align-items-center">
        	<div class="col-md-6">
                <div class="page-title">
            		<h1>Sign Up</h1>
                </div>
            </div> 
        </div>
    </div><!-- END CONTAINER-->
</div>
<!-- END SECTION BREADCRUMB -->

<!-- START MAIN CONTENT -->
<div class="main_content">

<!-- START SECTION SHOP -->
<div class="login_register_wrap section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-md-10">
                <div class="login_wrap">
                    <div class="padding_eight_all bg-white">
                        <div class="heading_s1">
                            <h3>Create an Account</h3>
                        </div>
                        <form method="post">
                            <div class="form-group">
                                <input type="text" required="" class="form-control" name="name" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group">
                                <input type="text" required="" class="form-control" name="email" placeholder="Enter Your Email">
                            </div>
                            <div class="form-group">
                                <input class="form-control" required="" type="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input class="form-control" required="" type="password" name="password" placeholder="Confirm Password">
                            </div>
                            <div class="login_footer form-group">
                                <div class="chek-form">
                                    <div class="custome-checkbox">
                                        <input class="form-check-input" type="checkbox" name="checkbox" id="exampleCheckbox2" value="">
                                        <label class="form-check-label" for="exampleCheckbox2"><span>I agree to terms &amp; Policy.</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-fill-out btn-block" name="register">Register</button>
                            </div>
                        </form>
                        <div class="different_login">
                            <span> or</span>
                        </div>
                        <ul class="btn-login list_none text-center">
                            <li><a href="#" class="btn btn-facebook"><i class="ion-social-facebook"></i>Facebook</a></li>
                            <li><a href="#" class="btn btn-google"><i class="ion-social-googleplus"></i>Google</a></li>
                        </ul>
                        <div class="form-note text-center">Already have an account? <a href="login.html">Log in</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION SHOP -->
<!-- END SECTION CONTACT -->
<?php include('include_footer.php') ?>
<!-- Latest jQuery --> 
<script src="js/js-jquery-1.12.4.min.js"></script>
<script src="js/js-popper.min.js"></script>
<script src="js/js-bootstrap.min.js"></script>
<script src="js/js-owl.carousel.min.js"></script>
<script src="js/js-magnific-popup.min.js"></script>
<script src="js/js-waypoints.min.js"></script>
<script src="js/js-parallax.js"></script>
<script src="js/js-jquery.countdown.min.js"></script>
<script src="js/js-imagesloaded.pkgd.min.js"></script>
<script src="js/js-isotope.min.js"></script>
<script src="js/js-jquery.dd.min.js"></script>
<script src="js/js-slick.min.js"></script>
<script src="js/js-jquery.elevatezoom.js"></script>
<!-- scripts js --><script src="js/js-scripts.js"></script>
</body>
</html>
