<!DOCTYPE html>
<html lang="en">
<head><!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="Anil z" name="author">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content=""><!-- SITE TITLE -->
    <title>Adeeta Online </title><!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png"><!-- Animation CSS -->
    <link rel="stylesheet" href="css/css-animate.css"><!-- Latest Bootstrap min CSS --><link rel="stylesheet" href="css/css-bootstrap.min.css"><!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet"><!-- Icon Font CSS -->
    <link rel="stylesheet" href="css/css-all.min.css">
    <link rel="stylesheet" href="css/css-ionicons.min.css">
    <link rel="stylesheet" href="css/css-themify-icons.css">
    <link rel="stylesheet" href="css/css-linearicons.css">
    <link rel="stylesheet" href="css/css-flaticon.css">
    <link rel="stylesheet" href="css/css-simple-line-icons.css"><!--- owl carousel CSS-->
    <link rel="stylesheet" href="css/css-owl.carousel.min.css">
    <link rel="stylesheet" href="css/css-owl.theme.css">
    <link rel="stylesheet" href="css/css-owl.theme.default.min.css"><!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="css/css-magnific-popup.css"><!-- Slick CSS -->
    <link rel="stylesheet" href="css/css-slick.css">
    <link rel="stylesheet" href="css/css-slick-theme.css"><!-- Style CSS -->
    <link rel="stylesheet" href="css/css-style.css">
    <link rel="stylesheet" href="css/css-responsive.css"> 

<!-- START HEADER -->
<?php include('include_header1.php') ?>
<!-- END HEADER --><!-- START SECTION BREADCRUMB -->
<div class="breadcrumb_section bg_gray page-title-mini">
    <div class="container"><!-- STRART CONTAINER -->
        <div class="row align-items-center">
        	<div class="col-md-6">
                <div class="page-title">
            		<h1>Privacy Policy</h1>
                </div>
            </div> 
        </div>
    </div><!-- END CONTAINER-->
</div>
<!-- END SECTION BREADCRUMB -->

<!-- START MAIN CONTENT -->
<div class="main_content">

<!-- START SECTION CONTACT -->
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="term_conditions">
                    <h6>Privacy Policy</h6>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose injected humour and the like</p>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    <hr><h6>Where does it come from?</h6>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose injected humour and the like</p>
                    <ol><li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur</li>
                        <li>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</li>
                        <li>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. </li>
                        <li>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.</li>
                    </ol><p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                    <hr><h6>Why do we use it?</h6>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words</p>
                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    <ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                        <li>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                        <li>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.</li>
                    </ul></div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION CONTACT --> 
<!-- END SECTION CONTACT -->
<?php include('include_footer.php') ?>
<!-- Latest jQuery --> 
<script src="js/js-jquery-1.12.4.min.js"></script>
<script src="js/js-popper.min.js"></script>
<script src="js/js-bootstrap.min.js"></script>
<script src="js/js-owl.carousel.min.js"></script>
<script src="js/js-magnific-popup.min.js"></script>
<script src="js/js-waypoints.min.js"></script>
<script src="js/js-parallax.js"></script>
<script src="js/js-jquery.countdown.min.js"></script>
<script src="js/js-imagesloaded.pkgd.min.js"></script>
<script src="js/js-isotope.min.js"></script>
<script src="js/js-jquery.dd.min.js"></script>
<script src="js/js-slick.min.js"></script>
<script src="js/js-jquery.elevatezoom.js"></script>
<!-- scripts js --><script src="js/js-scripts.js"></script>
</body>
</html>
