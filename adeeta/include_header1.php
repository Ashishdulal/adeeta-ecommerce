<header class="header_wrap">
    <div class="top-header light_skin bg_dark d-none d-md-block">
         <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-8">
                    <div class="header_topbar_info">
                        <div class="header_offer">
                            <span>Free Ground Shipping Over $250</span>
                        </div>
                        <div class="download_wrap">
                            <span class="mr-3">Coming Soon</span>
                            <ul class="icon_list text-center text-lg-left"><li><a href="category.php">
                                <i class="fab fa-apple"></i></a></li>
                                <li><a href="category.php"><i class="fab fa-android"></i></a></li> 
                            </ul></div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4">
                    <div class="d-flex align-items-center justify-content-center justify-content-md-end">
                          <ul class="social_icons text-center text-lg-left"><li><a href="category.php" class="sc_facebook"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="category.php" class="sc_twitter"><i class="ion-social-twitter"></i></a></li> 
                            <li><a href="category.php" class="sc_youtube"><i class="ion-social-youtube-outline"></i></a></li>
                            <li><a href="category.php" class="sc_instagram"><i class="ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="middle-header dark_skin">
    	<div class="container">
            <div class="nav_block">
               <a class="navbar-brand" href="index.php">
                    <img class="logo_light" src="images/logo.png" alt="logo"><img class="logo_dark" src="images/logo.png" alt="logo"></a>
                <div class="product_search_form radius_input search_form_btn">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="custom_select">
                                    <select class="first_null not_chosen"><option value="">All Category</option><option value="Dresses">Dresses</option><option value="Shirt-Tops">Shirt &amp; Tops</option><option value="T-Shirt">T-Shirt</option><option value="Pents">Pents</option><option value="Jeans">Jeans</option></select></div>
                            </div>
                            <input class="form-control" placeholder="Search Product..." required="" type="text"><button type="submit" class="search_btn3">Search</button>
                        </div>
                    </form>
                </div>
                <ul class="navbar-nav attr-nav align-items-center">
                    <li><a href="login.php" class="nav-link"><i class="linearicons-user"></i></a></li>
                    <li><a href="wishlist.php" class="nav-link"><i class="linearicons-heart"></i><span class="wishlist_count">0</span></a></li>
                    <li class="dropdown cart_dropdown"><a class="nav-link cart_trigger" href="category.php" data-toggle="dropdown"><i class="linearicons-bag2"></i><span class="cart_count">2</span><span class="amount"><span class="currency_symbol">$</span>159.00</span></a>
                        <div class="cart_box cart_right dropdown-menu dropdown-menu-right">
                            <ul class="cart_list"><li>
                                    <a href="category.php" class="item_remove"><i class="ion-close"></i></a>
                                    <a href="category.php"><img src="images/images-cart_thamb1.jpg" alt="cart_thumb1">Variable product 001</a>
                                    <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>78.00</span>
                                </li>
                                <li>
                                    <a href="category.php" class="item_remove"><i class="ion-close"></i></a>
                                    <a href="category.php"><img src="images/images-cart_thamb2.jpg" alt="cart_thumb2">Ornare sed consequat</a>
                                    <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>81.00</span>
                                </li>
                            </ul><div class="cart_footer">
                                <p class="cart_total"><strong>Subtotal:</strong> <span class="cart_price"> <span class="price_symbole">$</span></span>159.00</p>
                                <p class="cart_buttons"><a href="cart.php" class="btn btn-fill-line view-cart">View Cart</a><a href="checkout.php" class="btn btn-fill-out checkout">Checkout</a></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="bottom_header dark_skin main_menu_uppercase border-top">
    	<div class="container">
            <div class="row align-items-center"> 
            	<div class="col-lg-3 col-md-4 col-sm-6 col-3">
                    <div class="categories_wrap">
                        <button type="button" data-toggle="collapse" data-target="#navCatContent" aria-expanded="false" class="categories_btn categories_menu collapsed">
                            <span>All Categories </span><i class="linearicons-menu"></i>
                        </button>
                        <div id="navCatContent" class="navbar collapse" style="">
                            <ul>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-boss"></i> <span>Men's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner4.jpg" alt="menu_banner4"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-friendship"></i> <span>Kid's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-sunglasses"></i> <span>Accessories</span></a>
                                   <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li><li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-boss"></i> <span>Men's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner4.jpg" alt="menu_banner4"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-friendship"></i> <span>Kid's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-sunglasses"></i> <span>Accessories</span></a>
                                   <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                   <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-boss"></i> <span>Men's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner4.jpg" alt="menu_banner4"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-friendship"></i> <span>Kid's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-boss"></i> <span>Men's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner4.jpg" alt="menu_banner4"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-friendship"></i> <span>Kid's</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul></div>
                                </li>
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="category.php " data-toggle="dropdown"><i class="flaticon-sunglasses"></i> <span>Accessories</span></a>
                                   <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex"><li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex"><li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Featured Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vestibulum sed</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur tempus</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                        </ul></li>
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul><li class="dropdown-header">Popular Item</li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Curabitur laoreet</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Vivamus in tortor</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae facilisis</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Quisque condimentum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Etiam ac rutrum</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec vitae ante ante</a></li>
                                                            <li><a class="dropdown-item nav-link nav_item" href="category.php ">Donec porttitor</a></li>
                                                        </ul></li>
                                                </ul></li>
                                            <li class="mega-menu-col col-lg-5">
                                                <div class="header-banner2">
                                                    <a href="category.php "><img src="images/images-menu_banner5.jpg" alt="menu_banner5"></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>  
                            </ul> 
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-6 col-9">
                	<nav class="navbar navbar-expand-lg"><button class="navbar-toggler side_navbar_toggler" type="button" data-toggle="collapse" data-target="category.phpnavbarSidetoggle" aria-expanded="false"> 
                            <span class="ion-android-menu"></span>
                        </button>
                        <div class="pr_search_icon">
                            <a href="javascript:void(0);" class="nav-link pr_search_trigger"><i class="linearicons-magnifier"></i></a>
                        </div> 
                        <div class="collapse navbar-collapse mobile_side_menu" id="navbarSidetoggle">
                            <ul class="navbar-nav">
                                 
                                <li >
                                    <a class="nav-link nav_item" href="about.php" >About Adeeta</a>
                                    
                                </li>
                                <li  >
                                    <a class="nav-link nav_item" href="tnc.php " >Terms and Conditions</a> 
                                </li>
                                <li >
                                    <a class="nav-link nav_item" href="pp.php " >Privacy Policy</a>
                                     
                                </li> 
                                <li><a class="nav-link nav_item" href="contact.php ">Contact Us</a></li> 
                            </ul>
                        </div>
                        <div class="contact_phone contact_support">
                            <i class="linearicons-phone-wave"></i>
                            <span> +1 2033908059</span>
                        </div>
                    </nav></div>
            </div>
        </div>
    </div>
</header>