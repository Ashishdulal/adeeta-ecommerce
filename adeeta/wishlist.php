<!DOCTYPE html>
<html lang="en">
<head><!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="Anil z" name="author">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content=""><!-- SITE TITLE -->
    <title>Adeeta Online </title><!-- Favicon Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="images/fav.png"><!-- Animation CSS -->
    <link rel="stylesheet" href="css/css-animate.css"><!-- Latest Bootstrap min CSS --><link rel="stylesheet" href="css/css-bootstrap.min.css"><!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet"><!-- Icon Font CSS -->
    <link rel="stylesheet" href="css/css-all.min.css">
    <link rel="stylesheet" href="css/css-ionicons.min.css">
    <link rel="stylesheet" href="css/css-themify-icons.css">
    <link rel="stylesheet" href="css/css-linearicons.css">
    <link rel="stylesheet" href="css/css-flaticon.css">
    <link rel="stylesheet" href="css/css-simple-line-icons.css"><!--- owl carousel CSS-->
    <link rel="stylesheet" href="css/css-owl.carousel.min.css">
    <link rel="stylesheet" href="css/css-owl.theme.css">
    <link rel="stylesheet" href="css/css-owl.theme.default.min.css"><!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="css/css-magnific-popup.css"><!-- Slick CSS -->
    <link rel="stylesheet" href="css/css-slick.css">
    <link rel="stylesheet" href="css/css-slick-theme.css"><!-- Style CSS -->
    <link rel="stylesheet" href="css/css-style.css">
    <link rel="stylesheet" href="css/css-responsive.css"> 

<!-- START HEADER -->
<?php include('include_header1.php') ?>
<!-- END HEADER --><!-- START SECTION BREADCRUMB -->
<div class="breadcrumb_section bg_gray page-title-mini">
    <div class="container"><!-- STRART CONTAINER -->
        <div class="row align-items-center">
        	<div class="col-md-6">
                <div class="page-title">
            		<h1>Wishlist</h1>
                </div>
            </div> 
        </div>
    </div><!-- END CONTAINER-->
</div>
<!-- END SECTION BREADCRUMB -->

<!-- START MAIN CONTENT -->
<div class="main_content">

<!-- START SECTION SHOP -->
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive wishlist_table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name">Product</th>
                                <th class="product-price">Price</th> 
                                <th class="product-add-to-cart"></th>
                                <th class="product-remove">Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="product-thumbnail"><a href="#"><img src="images/images-cart_thamb1.jpg" alt="product1"></a></td>
                                <td class="product-name" data-title="Product"><a href="#">Blue Dress For Woman</a></td>
                                <td class="product-price" data-title="Price">$45.00</td>
                                
                                <td class="product-add-to-cart"><a href="#" class="btn btn-fill-out"><i class="icon-basket-loaded"></i> Add to Cart</a></td>
                                <td class="product-remove" data-title="Remove"><a href="#"><i class="ti-close"></i></a></td>
                            </tr>
                            <tr>
                                <td class="product-thumbnail"><a href="#"><img src="images/images-cart_thamb2.jpg" alt="product2"></a></td>
                                <td class="product-name" data-title="Product"><a href="#">Lether Gray Tuxedo</a></td>
                                <td class="product-price" data-title="Price">$55.00</td>
                                
                                <td class="product-add-to-cart"><a href="#" class="btn btn-fill-out"><i class="icon-basket-loaded"></i> Add to Cart</a></td>
                                <td class="product-remove" data-title="Remove"><a href="#"><i class="ti-close"></i></a></td>
                            </tr>
                            <tr>
                                <td class="product-thumbnail"><a href="#"><img src="images/images-cart_thamb1.jpg" alt="product3"></a></td>
                                <td class="product-name" data-title="Product"><a href="#">woman full sliv dress</a></td>
                                <td class="product-price" data-title="Price">$68.00</td>
                                 
                                <td class="product-add-to-cart"><a href="#" class="btn btn-fill-out"><i class="icon-basket-loaded"></i> Add to Cart</a></td>
                                <td class="product-remove" data-title="Remove"><a href="#"><i class="ti-close"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION SHOP -->
<!-- END SECTION CONTACT -->
<?php include('include_footer.php') ?>
<!-- Latest jQuery --> 
<script src="js/js-jquery-1.12.4.min.js"></script>
<script src="js/js-popper.min.js"></script>
<script src="js/js-bootstrap.min.js"></script>
<script src="js/js-owl.carousel.min.js"></script>
<script src="js/js-magnific-popup.min.js"></script>
<script src="js/js-waypoints.min.js"></script>
<script src="js/js-parallax.js"></script>
<script src="js/js-jquery.countdown.min.js"></script>
<script src="js/js-imagesloaded.pkgd.min.js"></script>
<script src="js/js-isotope.min.js"></script>
<script src="js/js-jquery.dd.min.js"></script>
<script src="js/js-slick.min.js"></script>
<script src="js/js-jquery.elevatezoom.js"></script>
<!-- scripts js --><script src="js/js-scripts.js"></script>
</body>
</html>
